
FlowRouter.route('/', {
    action: function() {
        FlowRouter.go('/login');
    }
});


/* Dashboard
*/
FlowRouter.route('/dashboard', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "dashboard"});
    }
});

/* Candidates
*/
FlowRouter.route('/candidates', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "candidates"});
    }
});

FlowRouter.route('/profileCandidate', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "profileCandidate"});
    }
});

FlowRouter.route('/profileCandidateEdit', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "profileCandidateEdit"});
    }
});

/* Employees
*/
FlowRouter.route('/employees', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "employees"});
    }
});

FlowRouter.route('/profile', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "profile"});
    }
});

/* Projects
*/
FlowRouter.route('/projects', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "projects"});
    }
});

FlowRouter.route('/projects/:id', {
    action: function(params) {
        var project = _.find(Projects, function(project) {
            return (project.id === params.id);
        });
        BlazeLayout.render("mainLayout", {content: "projectDetail", data: project});
    }
});

FlowRouter.route('/projectDetail', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "projectDetail"});
    }
});

/* Interviews
*/
FlowRouter.route('/interviews', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "interviews"});
    }
});

/* Clients
*/
FlowRouter.route('/clients', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "clients"});
    }
});

FlowRouter.route('/clients/add', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "clientsEdit", type: "add"});
    }
});

FlowRouter.route('/clients/:id', {
    action: function(params) {
        var client = _.find(Clients, function(cliente) {
            return (cliente.id === params.id);
        });
        BlazeLayout.render("mainLayout", {content: "clientsEdit", data: client});
    }
});




/* Reports
*/
FlowRouter.route('/reports', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "reports"});
    }
});

/* Configuration
*/
FlowRouter.route('/configuration', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "configuration"});
    }
});


//
// Other pages routes
//


FlowRouter.route('/index', {
    action: function() {
        BlazeLayout.render("blankLayout", {content: "index"});
    }
});

FlowRouter.route('/login', {
    action: function() {
        BlazeLayout.render("blankLayout", {content: "login"});
    }
});


FlowRouter.route('/forgotPassword', {
    action: function() {
        BlazeLayout.render("blankLayout", {content: "forgotPassword"});
    }
});

FlowRouter.notFound = {
    action: function() {
        BlazeLayout.render("blankLayout", {content: "notFound"});
    }
};



FlowRouter.route('/candidates1', {
    action: function() {
        BlazeLayout.render("mainLayout", {content: "candidates1"});
    }
});