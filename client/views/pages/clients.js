Template.clients.rendered = function(){

    // Add slimscroll to element
    $('.full-height-scroll').slimscroll({
        height: '100%'
    })

    $('.tab-content .tab-pane:first-child').addClass('active'); 

};

Template.clients.helpers({
  clients: function() {
    return Clients;
  },
  clientsId: function(id) {
    return this.id === id;
  },
})