Template.dashboard.rendered = function(){

 	// Initialize fooTable
    $('.footable').footable();
    $('.footable2').footable();
};


Template.dashboard.helpers({

    // Example empleados
	employees : [
        {id: 1, avatar: 'img/a1.jpg', first_name: 'Maria', last_name: 'Gonzalez', project: '3'},
        {id: 2, avatar: 'img/a1.jpg', first_name: 'Augusto', last_name: 'Martinez', project: '2'},
        {id: 3, avatar: 'img/a1.jpg', first_name: 'Pedro', last_name: 'Mendoza', project: '3'},
        {id: 4, avatar: 'img/a1.jpg', first_name: 'Maria', last_name: 'Teran', project: '3'},
        {id: 5, avatar: 'img/a1.jpg', first_name: 'Adriana', last_name: 'Cardoso', project: '3'}
    ],
    // Example empleados
	projects : [
        {id: 1, name: 'AAA', status: 'Planificacion', members: '3'},
        {id: 2, name: 'BBB', status: 'Finalizado', members: '4'},
        {id: 3, name: 'CCC', status: 'Desarrollo', members: '3'},
        {id: 4, name: 'III', status: 'Desarrollo', members: '2'},
        {id: 5, name: 'MMM', status: 'Desarrollo', members: '3'}
    ]


});