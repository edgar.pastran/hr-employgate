Template.employees.rendered = function(){

    // Initialize fooTable
    $('.footable').footable();

};

Template.employees.helpers({
	employees : [
        {id: 1, avatar: 'img/a1.jpg', first_name: 'Maria', last_name: 'Gonzalez', profession: 'Graphics designer', location: 'Venezuela', email: 'nombre@employgate.com', project: '3'},
        {id: 2, avatar: 'img/a1.jpg', first_name: 'Augusto', last_name: 'Martinez', profession: 'Marketing manager', location: 'Venezuela', email: 'nombre@employgate.com', project: '3'},
        {id: 3, avatar: 'img/a1.jpg', first_name: 'Pedro', last_name: 'Mendoza', profession: 'Marketing manager', location: 'Venezuela',  email: 'nombre@employgate.com', project: '3'},
        {id: 4, avatar: 'img/a1.jpg', first_name: 'Maria', last_name: 'Teran', profession: 'Sales manager', location: 'Venezuela', email: 'nombre@employgate.com', project: '3'},
        {id: 5, avatar: 'img/a1.jpg', first_name: 'Adriana', last_name: 'Cardoso', profession: 'Marketing manager', location: 'Venezuela', email: 'nombre@employgate.com', project: '3'}
    ]
})


    