Template.login.rendered = function(){

    // Set validation login
    $("#formLogin").validate({
        rules: {
            password: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            email: {
                required: "Custom message for required",
                email: "Custom message for proper email address"
            }
        }
    });


};
