Template.profileCandidate.rendered = function(){

    $("#sparkline1").sparkline([34, 43, 43, 35, 44, 32, 44, 48], {
        type: 'line',
        width: '100%',
        height: '50',
        lineColor: '#1ab394',
        fillColor: "transparent"
    });

    $('#tab_b_1').hide();



    var $image = $(".image-crop > img")
    $($image).cropper({
        aspectRatio: 1/1,
        preview: ".img-preview",
        done: function(data) {
            // Output the result data for cropping image.
        }
    });



    var $inputImage = $("#inputImage");
    if (window.FileReader) {
        $inputImage.change(function() {
            var fileReader = new FileReader(),
                    files = this.files,
                    file;

            if (!files.length) {
                return;
            }

            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {
                fileReader.readAsDataURL(file);
                fileReader.onload = function () {
                    $inputImage.val("");
                    console.log($inputImage.val(""));
                    console.log(this.result);
                    $image.cropper("reset", true).cropper("replace", this.result);

                    
                    $('#myModal').modal({backdrop: 'static', keyboard: false});
                    $('#myModal').modal('show');
                };
            } else {
                showMessage("Please choose an image file.");
            }
        });
    } else {
        $inputImage.addClass("hide");


    }


    $("#cerrar").click(function() {
        
        $image.cropper("reset", true).cropper("replace", 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAMAUExURf///wAAAJWVlfHx8YKCgvb29vz8/N7e3vX19e7u7nh4eNfX15mZma2trSUlJSsrKzAwMOjo6Kenp8LCwlxcXMvLy9PT07GxsVBQUBwcHGFhYbu7uyEhIRMTE2dnZ3BwcDs7O0NDQ4yMjBYWFn5+fkFBQaCgoDc3N0lJSVNTUyoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fX5+fn9/f4CAgIGBgYKCgoODg4SEhIWFhYaGhoeHh4iIiImJiYqKiouLi4yMjI2NjY6Ojo+Pj5CQkJGRkZKSkpOTk5SUlJWVlZaWlpeXl5iYmJmZmZqampubm5ycnJ2dnZ6enp+fn6CgoKGhoaKioqOjo6SkpKWlpaampqenp6ioqKmpqaqqqqurq6ysrK2tra6urq+vr7CwsLGxsbKysrOzs7S0tLW1tba2tre3t7i4uLm5ubq6uru7u7y8vL29vb6+vr+/v8DAwMHBwcLCwsPDw8TExMXFxcbGxsfHx8jIyMnJycrKysvLy8zMzM3Nzc7Ozs/Pz9DQ0NHR0dLS0tPT09TU1NXV1dbW1tfX19jY2NnZ2dra2tvb29zc3N3d3d7e3t/f3+Dg4OHh4eLi4uPj4+Tk5OXl5ebm5ufn5+jo6Onp6erq6uvr6+zs7O3t7e7u7u/v7/Dw8PHx8fLy8vPz8/T09PX19fb29vf39/j4+Pn5+fr6+vv7+/z8/P39/f7+/v///4UXbCEAAAGTSURBVHja7JjdcoJADEbziYIoIuAv2lGHzjj6/i/Ym960o7DsJt8U63kAziRLdpOIvHnTRRyVq+s1q7apzffTHD+ZqSsaPEBXc8UTxnqOO56j5TihjTLWcKzQjkoggL0lBcGy75ZM7LOlUS8OkuB87RiSg4skJWQLFUOyZ0gwAMmEIYkYkvRlJNOX+bukdnGsAyWZiyRjXPXzIbwnbgc/ZUjKAUhOjDrZ4M+c/CAkYwdHQ+iFQ8vELV+E0QG38DkoIwTSHUpCmLUqFYfM7WfGrlAYktx8IwFgJ4RQ9BYfB4LjeSgfDMmdITlpSnL7hVfLy7UVwi+8lAHVSdwqUTqVuP2qLyiru4bgABahCkabCkcC3vkjnPn0XUTO0IeRx5bgBg96dXkL+HK0VwDAykVxQSjr9qwlNXQoUr9OsS8PJ+IztGl+pW16gQWH2OP28GDebyPgCSEQrqQ0lUTdk0E4351GAUK+8AKSo4hIYizJXfdAwfmqGRJrB86dra5OKBuG5GwvSeyPBKgZEsiIgLz5n3wNAIEwGb/cZm7OAAAAAElFTkSuQmCC');

    });


    $("#cerrar1").click(function() {
        
        $image.cropper("reset", true).cropper("replace", 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAMAUExURf///wAAAJWVlfHx8YKCgvb29vz8/N7e3vX19e7u7nh4eNfX15mZma2trSUlJSsrKzAwMOjo6Kenp8LCwlxcXMvLy9PT07GxsVBQUBwcHGFhYbu7uyEhIRMTE2dnZ3BwcDs7O0NDQ4yMjBYWFn5+fkFBQaCgoDc3N0lJSVNTUyoqKisrKywsLC0tLS4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQkNDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1hYWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1tbW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fX5+fn9/f4CAgIGBgYKCgoODg4SEhIWFhYaGhoeHh4iIiImJiYqKiouLi4yMjI2NjY6Ojo+Pj5CQkJGRkZKSkpOTk5SUlJWVlZaWlpeXl5iYmJmZmZqampubm5ycnJ2dnZ6enp+fn6CgoKGhoaKioqOjo6SkpKWlpaampqenp6ioqKmpqaqqqqurq6ysrK2tra6urq+vr7CwsLGxsbKysrOzs7S0tLW1tba2tre3t7i4uLm5ubq6uru7u7y8vL29vb6+vr+/v8DAwMHBwcLCwsPDw8TExMXFxcbGxsfHx8jIyMnJycrKysvLy8zMzM3Nzc7Ozs/Pz9DQ0NHR0dLS0tPT09TU1NXV1dbW1tfX19jY2NnZ2dra2tvb29zc3N3d3d7e3t/f3+Dg4OHh4eLi4uPj4+Tk5OXl5ebm5ufn5+jo6Onp6erq6uvr6+zs7O3t7e7u7u/v7/Dw8PHx8fLy8vPz8/T09PX19fb29vf39/j4+Pn5+fr6+vv7+/z8/P39/f7+/v///4UXbCEAAAGTSURBVHja7JjdcoJADEbziYIoIuAv2lGHzjj6/i/Ym960o7DsJt8U63kAziRLdpOIvHnTRRyVq+s1q7apzffTHD+ZqSsaPEBXc8UTxnqOO56j5TihjTLWcKzQjkoggL0lBcGy75ZM7LOlUS8OkuB87RiSg4skJWQLFUOyZ0gwAMmEIYkYkvRlJNOX+bukdnGsAyWZiyRjXPXzIbwnbgc/ZUjKAUhOjDrZ4M+c/CAkYwdHQ+iFQ8vELV+E0QG38DkoIwTSHUpCmLUqFYfM7WfGrlAYktx8IwFgJ4RQ9BYfB4LjeSgfDMmdITlpSnL7hVfLy7UVwi+8lAHVSdwqUTqVuP2qLyiru4bgABahCkabCkcC3vkjnPn0XUTO0IeRx5bgBg96dXkL+HK0VwDAykVxQSjr9qwlNXQoUr9OsS8PJ+IztGl+pW16gQWH2OP28GDebyPgCSEQrqQ0lUTdk0E4351GAUK+8AKSo4hIYizJXfdAwfmqGRJrB86dra5OKBuG5GwvSeyPBKgZEsiIgLz5n3wNAIEwGb/cZm7OAAAAAElFTkSuQmCC');

    });


    $("#download").click(function() {
        window.open($image.cropper("getDataURL"));
    });

    $("#zoomIn").click(function() {
        $image.cropper("zoom", 0.1);
    });

    $("#zoomOut").click(function() {
        $image.cropper("zoom", -0.1);
    });

    $("#rotateLeft").click(function() {
        $image.cropper("rotate", 45);
    });

    $("#rotateRight").click(function() {
        $image.cropper("rotate", -45);
    });

    $("#setDrag").click(function() {
        $image.cropper("setDragMode", "crop");
    });





};



Template.profileCandidate.events({
  'click #tab-profile1': function(event) {
    $('#tab_a_1').show();
    $('#tab_b_1').hide();
  },
    'click #tab-profile2': function(event) {
    $('#tab_b_1').show();
    $('#tab_a_1').hide();
  }
});    