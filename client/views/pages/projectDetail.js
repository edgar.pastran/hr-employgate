Template.projectDetail.rendered = function(){


};

Template.projectDetail.helpers({
  projects: function() {
    return Projects;
  },
  statusIs: function(status) {
    return 'Active' === status;
  },
})