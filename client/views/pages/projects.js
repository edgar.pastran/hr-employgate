Template.projects.rendered = function(){

};

Template.projects.helpers({
  projects: function() {
    return Projects;
  },
  statusIs: function(status) {
    return this.status === status;
  },
})