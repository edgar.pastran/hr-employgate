Clients = [
	{
		id: '1',
		name: 'Tellus Institute',
		description: 'Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on.',
		location: 'Angola',
		logo: 'poster1.png',
		note: 'primera nota',
		contacts: [{name:'adri', phone:'58 426 234 23 33 ', email:''}, {name:'card', phone:'', email:'asad@gmail.com'}],
		web: 'http://cliente.com',
		email : 'cliente@gmail.com',
		phone: '',
		address: '',
		projects: [{id: '1', name: 'Nombre proyecto', created: '11.08.2014'}],
		status: 'Active'
	},
	{
		id: '2',
		name: 'Poster: Star Wars 2',
		description: '',
		location: 'Luxembourg',
		logo: 'poster2.png',
		note: 'nota especial',
		contacts: [{name:'Nombre', phone:'58 426 234 23 23', email:'name@gmail.com'}],
		web: '',
		email : '',
		phone: '',
		address: '',
		projects: [{id: '2', name: 'Nombre proyecto', created: '11.08.2014'},{id: '3', name: 'Nombre proyecto', created: '11.08.2014'}],
		status: 'Active'
	},
	{
		id: '3',
		name: 'Poster: Star Wars 3',
		description: '',
		location: 'Philippines',
		logo: 'poster3.png',
		note: '',
		contacts: [{name:'Nombre', phone:'58 426 234 23 23', email:'name@gmail.com'}],
		web: '',
		email : '',
		phone: '',
		address: '',
		projects: [{id: '2', name: 'Nombre proyecto', created: '11.08.2014'}],
		status: 'Active'
	},
	{
		id: '4',
		name: 'Poster: Star Wars 4',
		description: '',
		location: 'Korea, North',
		logo: 'poster4.png',
		note: '',
		contacts: [{name:'Nombre', phone:'58 426 234 23 23', email:'name@gmail.com'}],
		web: '',
		email : '',
		phone: '',
		address: '',
		projects: [],
		status: 'Active'
	}
];

