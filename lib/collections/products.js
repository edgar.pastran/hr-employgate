Products = [
	{
		id: '1',
		title: 'Poster: Star Wars 1',
		price: 123,
		image: 'poster1.png'
	},
	{
		id: '2',
		title: 'Poster: Star Wars 2',
		price: 1234,
		image: 'poster2.png'
	},
	{
		id: '3',
		title: 'Poster: Star Wars 3',
		price: 12345,
		image: 'poster3.png'
	},
	{
		id: '4',
		title: 'Poster: Star Wars 4',
		price: 123456,
		image: 'poster4.png'
	}
];

