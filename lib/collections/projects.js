Projects = [
	{
		id: '1',
		name: 'Project 1',
		logo: 'logo.png',
		description: 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which dont look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isnt anything embarrassing',
		created: '11.08.2014',
		created_by: 'Nombre Persona',
		client: {id:'1', name:'Tellus Institute'},
		members: [{id:'1', name:'', avatar:'img/a1.jpg', start_date: '', end_date: '', role:''},
				  {id:'3', name:'', avatar:'img/a1.jpg', start_date: '', end_date: '', role:''},
				  {id:'4', name:'', avatar:'img/a1.jpg', start_date: '', end_date: '', role:''},
				  {id:'5', name:'', avatar:'img/a1.jpg', start_date: '', end_date: '', role:''},
				  {id:'6', name:'', avatar:'img/a1.jpg', start_date: '', end_date: '', role:''},
				  {id:'7', name:'', avatar:'img/a1.jpg', start_date: '', end_date: '', role:''},
				  {id:'8', name:'', avatar:'img/a1.jpg', start_date: '', end_date: '', role:''},
				  {id:'9', name:'', avatar:'img/a1.jpg', start_date: '', end_date: '', role:''},
				  {id:'10', name:'', avatar:'img/a1.jpg', start_date: '', end_date: '', role:''},
				  {id:'11', name:'', avatar:'img/a1.jpg', start_date: '', end_date: '', role:''}],
		status: 'Active',
		tags: [{name:'hola'},{name:'epa'},{name:'hey'}]
	},
	{
		id: '2',
		name: 'Project 2',
		logo: 'logo.png',
		description: 'Este project es algo muy importante',
		created: '11.08.2014',
		created_by: 'Nombre Persona',
		client: {id:'2', name:'Poster: Star Wars 2'},
		members: [{id:'1', name:'', avatar:'img/a1.jpg', start_date: '', end_date: '', role:''},
				  {id:'2', name:'', avatar:'img/a1.jpg', start_date: '', end_date: '', role:''}],
		status: 'Unactive',
		tags: [{name:'aa'},{name:'ee'},{name:'ii'}]
	}
];

